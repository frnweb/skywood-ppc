# Skywood Recovery PPC

This template was build with Zurb Foundation for Sites, using their Panini static site generator.

## Usage

To update the site, run the following commands:

```
git clone https://bitbucket.org/frnweb/skywood-ppc.git
cd skywood-ppc
yarn
yarn run start
```

This kick-starts the dev server for you to mess around with. See Zurb's documentation for more details.

## Deployment

To get a full deploy, `cd` into your repo and run the following `yarn run build`. This will create a build in the `dist` directory that you can deploy to any server that can serve static HTML.
