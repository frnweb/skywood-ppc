import 'babel-polyfill'
import $ from 'jquery'
import whatInput from 'what-input'
//
import googleAnalytics from './lib/google-analytics'
import liveHelpNow from './lib/live-help-now'
import googleMaps from './lib/google-maps'
import carousels from './lib/carousels'
import stickyNav from './lib/sticky-nav'
import smoothScroll from './lib/smooth-scroll'
import mobileMenuToggler from './lib/mobile-menu-toggler'
import hamburgerImageToggler from './lib/hamburger-image-toggler'
import './lib/hot-jar'
import './lib/dialog-tech'

window.$ = $

import Foundation from 'foundation-sites'
// // If you want to pick and choose which modules to include, comment out the above and uncomment
// // the line below
// //import './lib/foundation-explicit-pieces';

$(document).foundation()

// //------------------//
// // Google Analytics //
// //------------------//
googleAnalytics()

// //---------------------//
// // Live Chat Now Setup //
// //---------------------//
liveHelpNow()

// //-------------//
// // Google Maps //
// //-------------//
if (Promise !== 'undefined') {
  googleMaps(() => {
    const hidden = ['water', 'road.local', 'poi.park'].map(item => ({
      featureType: item,
      stylers: [{ visibility: 'off' }],
    }))

    return [...hidden]
  })
}

// //----------------//
// // Carousel Setup //
// //----------------//

// // Facility Slider
// //----------------
const smallSettings = {
  breakpoint: 767,
  settings: {
    slidesToShow: 1,
    slidesToScroll: 1,
    centerPadding: null,
  },
}
const mediumSettings = {
  breakpoint: 1600,
  settings: {
    slidesToShow: 2,
    slidesToScroll: 2,
  },
}
const largeSettings = {
  slidesToScroll: 4,
  slidesToShow: 4,
}
carousels('#facility-slider-slick', {
  ...largeSettings,
  responsive: [smallSettings, mediumSettings],
})

// // Our Mission Slider
// //-------------------
carousels('#our-mission-slider', {
  dots: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  centerMode: false,
  arrows: false,
})

// //--------------//
// // Header setup //
// //--------------//
const headerSelector = '#header-bar'
const headerMobileMenuSelector = `${headerSelector}-mobile-menu`
const headerMenuSelector = `${headerSelector}__responsive-menu`

// // Sticky Nav
// //-------------
stickyNav(headerSelector)

// // Smooth Scroller
// //------------------
smoothScroll(headerSelector)

// // Toggle Menu on Select
// //----------------------
mobileMenuToggler(
  headerMenuSelector,
  headerMobileMenuSelector,
  headerMenuSelector,
  `.header-bar__title-bar-title`
)

// // Hamburger Helper
// //-----------------
hamburgerImageToggler()
