import $ from 'jquery'
import { Observable } from 'rxjs/Observable'
import { AsyncSubject } from 'rxjs/AsyncSubject'
import 'rxjs/add/observable/interval'
import 'rxjs/add/observable/fromEvent'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/mapTo'
import 'rxjs/add/operator/skipUntil'
import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/mergeMap'
import 'rxjs/add/operator/switchMap'
import 'rxjs/add/operator/concatMap'
import 'rxjs/add/operator/first'
import 'rxjs/add/operator/do'
//
import config from './config'
//

export const ready$ = new AsyncSubject()

$(document).ready(() => {
  ready$.next(true)
  ready$.complete()
})

export const onlineStatus$ = ready$
  .switchMap(() => Observable.interval(100))
  // .map(() => !!(!!window.lhnJsSdk && window.lhnJsSdk.isOnline))
  .map(() => {
    if (
      typeof window.lhnJsSdk === 'undefined' ||
      typeof window.lhnJsSdk.isOnline === 'undefined'
    ) {
      return undefined
    }

    return window.lhnJsSdk.isOnline
  })
  .distinctUntilChanged()

export const buttonClick$ = onlineStatus$
  .filter(isOnline => !isOnline)
  .switchMap(() =>
    Observable.fromEvent(
      document.querySelectorAll(config.buttonSelector),
      'click'
    )
  )

export const chatWindowActive$ = onlineStatus$
  .filter(isOnline => !isOnline)
  .switchMap(() => Observable.interval(1000))
  .map(() => $(config.windowSelector))
  .filter(emptyQueryFilter)
  .switchMap($el =>
    Observable.interval(250).map(() => $el.hasClass(config.windowActiveClass))
  )
  .distinctUntilChanged()

export const preChatActive$ = chatWindowActive$
  .filter(isActive => !isActive)
  .switchMap(() => Observable.interval(250))
  .map(() => $(config.preChatFormSelector))
  .filter(emptyQueryFilter)
  .switchMap(monitorVisibility)
  .distinctUntilChanged()

function emptyQueryFilter($el) {
  return $el.length > 0
}

function monitorVisibility($el) {
  return Observable.interval(250).map(() => $el.is(':visible'))
}
