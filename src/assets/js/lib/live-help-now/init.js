window.lhnJsSdkInit = function() {
  lhnJsSdk.setup = {
    application_id: '2cb55594-3a4a-4b89-bf4b-9f1a6cfdbff3',
    application_secret: '57d499aba56340a4a6916b981ad582f9b98f322ee04641a0bc',
  }
  lhnJsSdk.controls = [
    {
      type: 'hoc',
      id: 'a3814cc9-0942-4a9c-b1b5-653d2e9cdb8f',
    },
  ]
  lhnJsSdk.dictionary = {
    agentConnecting: 'Connecting to agent',
    callbackMenu: 'Callback',
    callbackTitle: 'Request a callback',
    cancel: 'Cancel',
    chatMenu: 'Chat',
    chatTitle: 'Conversation',
    email: 'Email',
    endChat: 'End Chat',
    endChatConfirm: 'Are you sure you want to end the current chat?',
    inviteCancel: 'Dismiss',
    inviteStart: 'Chat now',
    knowledgeMenu: 'FAQ',
    knowledgeTitle: 'Search Knowledge',
    livechat: 'LIVE CHAT',
    livechat_offline: 'GET HELP',
    newChatTitle: 'New Conversation',
    offlineTitle: 'Leave a message',
    send: 'Send',
    startChat: 'Start Chat',
    submit: 'Submit',
    surveyTitle: 'Survey',
    ticketMenu: 'Ticket',
    ticketTitle: 'Submit a ticket',
  }
}
;(function(d, s) {
  var newjs,
    lhnjs = d.getElementsByTagName(s)[0]
  newjs = d.createElement(s)
  newjs.src =
    'https://developer.livehelpnow.net/js/sdk/lhn-jssdk-current.min.js'
  lhnjs.parentNode.insertBefore(newjs, lhnjs)
})(document, 'script')
