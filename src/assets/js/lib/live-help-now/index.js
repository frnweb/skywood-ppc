import $ from 'jquery'
//
import './init'
import * as observable from './observables'
import config from './config'

const toggleButtonVisibility = isOnline => {
  const $buttons = $(config.buttonSelector).map((index, item) => {
    const $item = $(item)
    const $parent = $item.parent(config.fancyButtonParentSelector)
    return $parent.length > 0 ? $parent : $item
  })

  $buttons.each((index, item) => {
    const $item = $(item)
    if (isOnline === true || isOnline === false) {
      $item.fadeIn(500)
    } else {
      $item.hide(0)
    }
  })
}

const changeButtonText = isOnline => {
  const $buttons = $(config.buttonSelector)

  $buttons.each((index, item) => {
    const $item = $(item)

    $item.html((index, str) => {
      const regex = new RegExp(
        `${isOnline ? config.offlineText : config.onlineText}`,
        'gi'
      )
      return str.replace(
        regex,
        isOnline ? config.onlineText : config.offlineText
      )
    })
  })
}

const openChatWindow = e => {
  !!e && e.preventDefault()
  window.lhnJsSdk && window.lhnJsSdk.openHOC()
}

const logStates = () => {
  observable.ready$.subscribe(x => console.log('is ready', x))
  observable.onlineStatus$.subscribe(x => console.log('is online', x))
  observable.chatWindowActive$.subscribe(x => console.log('chat active', x))
  observable.preChatActive$.subscribe(x => console.log('pre chat active', x))
}

const addWaitTimeText = () => {
  const $form = $(config.preChatFormSelector)
    .children('div:first-child')
    .filter((index, item) => $(item).find('.lhnWaitTime').length === 0)
    .prepend('<p class="lhnWaitTime">Average Wait Time: 4 minutes</p>')
}

export default () => {
  // logStates()
  observable.buttonClick$.subscribe(openChatWindow)
  observable.onlineStatus$.subscribe(toggleButtonVisibility)
  observable.onlineStatus$.subscribe(changeButtonText)
  observable.preChatActive$.subscribe(addWaitTimeText)
}
