const config = {
  buttonSelector: '[data-chat]',
  windowSelector: '#lhnHelpOutCenter',
  windowActiveClass: 'lhnActive',
  fancyButtonParentSelector: '.fancy-button__parent',
  preChatFormSelector: '.lhnPreChatForm',
  onlineText: 'Live Chat',
  offlineText: 'Email Us',
}

export default Object.freeze(config)
