import $ from 'jquery'
import Foundation from 'foundation-sites'

export default selector => {
  $(document).ready(() => {
    $(selector)
      .find('a')
      .each((index, item) => {
        const $el = $(item)
        const shouldSmoothScroll = /^#.+?$/.test($el.attr('href'))

        if (shouldSmoothScroll) {
          new Foundation.SmoothScroll($el, {
            offset: 100,
            animationEasing: 'swing',
            animationDuration: 1000,
            threshold: 100,
          })
        }
      })
  })
}
