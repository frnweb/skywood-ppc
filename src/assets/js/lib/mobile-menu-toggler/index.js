import $ from 'jquery'

const clickHandler = $togglee => e => {
  // $togglee.filter(':visible').each(() => {
  $togglee.hide()
  // })
}

const manageHandlers = ($mobileMenu, $togglers, $togglee) => {
  const handler = clickHandler($togglee)

  if ($mobileMenu.is(':visible')) {
    $togglers.on('click', handler)
  } else {
    $togglers.off('click')
  }
}

const mobileMenuToggler = (
  itemToToggle,
  mobileMenuSelector,
  ...itemsThatTrigger
) => {
  const togglersSelector = itemsThatTrigger.join(', ')

  $(document).ready(() => {
    const $togglee = $(itemToToggle)
    const $mobileMenu = $(mobileMenuSelector)
    const $togglers = $(togglersSelector)

    manageHandlers($mobileMenu, $togglers, $togglee)

    $(window).on('resize', () => {
      manageHandlers($mobileMenu, $togglers, $togglee)
    })
  })
}

export default mobileMenuToggler
