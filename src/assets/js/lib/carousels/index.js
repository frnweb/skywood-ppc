import $ from 'jquery'
import 'slick-carousel'

export default (selector, options) => {
  const defaultOptions = {
    prevArrow: `<button class="slick-prev"></button>`,
    nextArrow: `<button class="slick-next"></button>`,
    slidesToShow: 2,
    slidesToScroll: 2,
    centerMode: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    centerPadding: '15vw',
  }

  const slickOptions = { ...defaultOptions, ...options }

  $(selector).slick(slickOptions)
}
