import $ from 'jquery'

const hamburgerImageToggler = (
  hamburgerSelector = '#header-bar__hamburger',
  menuSelector = '#header-bar__responsive-menu',
  parentSelector = '#header-bar',
  toggleClass = 'is-active'
) => {
  $(document).ready(() => {
    const $menu = $(menuSelector)
    const $hamburger = $(hamburgerSelector)

    $hamburger.on('click', () => {
      if ($menu.is(':visible')) {
        $hamburger.addClass(toggleClass)
      } else {
        $hamburger.removeClass(toggleClass)
      }
    })

    $menu
      .parents(parentSelector)
      .find(`a, button`)
      .not(hamburgerSelector)
      .on('click', () => {
        $hamburger.removeClass(toggleClass)
      })
  })
}

export default hamburgerImageToggler
